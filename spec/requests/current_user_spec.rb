require 'rails_helper'
require 'devise/jwt/test_helpers'

RSpec.describe "CurrentUsers", type: :request do
  describe "GET /" do
    user = FactoryBot.create(:user)
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, user)

    context "from login user" do
      it "should return 200:OK" do
        get '/api/v1/current_user', headers: auth_headers
        expect(response).to have_http_status(:success)
      end
    end
  end
end
