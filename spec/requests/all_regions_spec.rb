require 'rails_helper'

RSpec.describe "AllRegions", type: :request do
  let!(:regions) { FactoryBot.create_list(:region, 20)}

  before { get '/api/v1/regions' }

  it "returns all regions" do
    expect(JSON.parse(response.body).size).to eq(20)
  end

  it "returns status of code 200" do
    expect(response).to have_http_status(:success)
  end
end
