require 'rails_helper'

RSpec.describe Region, type: :model do

  subject {
    described_class.new(
      title: "Title one",
      country_details: "info here",
      tax_details: "info here",
      currency_details: "info here"
    )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a title" do
    subject.title = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a country_details" do
    subject.country_details = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a currency_details" do
    subject.currency_details = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a tax_details" do
    subject.tax_details = nil
    expect(subject).to_not be_valid
  end

  describe "Associations" do
    it { should have_many(:products) }
  end
end
