require 'rails_helper'

RSpec.describe Order, type: :model do
  describe "Validations" do
    it { should validate_presence_of :full_name }
    it { should validate_presence_of :email }
  end

  describe "Associations" do
    it { should belong_to(:customer) }
    it { should have_many(:order_items) }
  end
end
