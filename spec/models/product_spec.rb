require 'rails_helper'

RSpec.describe Product, type: :model do
  subject {
    described_class.new(
      title: "Title one",
      description: "Description here",
      price: 5000,
      stock_number: 4,
      sku: "shvdwy38c",
      region_id: 5
    )
  }

  it "it is not valid without a title" do
    subject.title = nil
    expect(subject).to_not be_valid
  end

  it "it is not valid without a description" do
    subject.description = nil
    expect(subject).to_not be_valid
  end

  it "it is not valid without a price" do
    subject.price = nil
    expect(subject).to_not be_valid
  end

  it "it is not valid without a stock_number" do
    subject.stock_number = nil
    expect(subject).to_not be_valid
  end

  it "it is not valid without a sku" do
    subject.sku = nil
    expect(subject).to_not be_valid
  end

  describe "Associations" do
    it { should belong_to(:region) }
  end
end
