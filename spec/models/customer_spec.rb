require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe "Validations" do
    it { should validate_uniqueness_of :email }
  end

  describe "Associations" do
    it { should belong_to(:user).without_validating_presence }
    it { should have_many(:orders) }
  end
end
