FactoryBot.define do
  factory :region do
    title { Faker::FunnyName.name }
    country_details { Faker::TvShows::RickAndMorty.quote }
    tax_details { Faker::TvShows::RickAndMorty.character }
    currency_details { Faker::TvShows::RickAndMorty.location }
  end
end
