FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Barcode.ean(8) }
    confirmed_at { DateTime.now }
  end
end
