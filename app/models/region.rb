class Region < ApplicationRecord
  has_many :products, dependent: :destroy

  # Validations
  validates_presence_of :title, :country_details, :currency_details, :tax_details
end
