class Product < ApplicationRecord
  belongs_to :region

  # Validations
  validates_presence_of :title, :price, :description, :stock_number, :sku
end
